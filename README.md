# The Movies APP

## Download

- $ git clone https://neatorresca@bitbucket.org/neatorresca/masmovies.git
- $ cd masmovies

## Install

$ npm install

## Run locally

$ ng serve -o

## Run tests

$ ng test

## Additional notes

- Customized component is TOP 5 Movies
- Unit testing cases include:

1. Top movies component to be correctly initialized
2. Movie list component to be correctly initialized
3. Movie list is empty at start of the application
4. Mock movie is correctly created and then movie list array is updated