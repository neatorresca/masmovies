import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { FormsModule } from '@angular/forms'
import { AppRoutingModule } from './app-routing.module'
import { HttpClientModule } from '@angular/common/http'

/*COMPONENTS*/
import { AppComponent } from './app.component'
import { MoviesComponent } from './movies/movies.component'
import { HeaderComponent } from './header/header.component'
import { MovieListComponent } from './movies/movie-list/movie-list.component'
import { MovieTopComponent } from './movies/movie-top/movie-top.component'
import { MovieItemComponent } from './movies/movie-list/movie-item/movie-item.component'
import { MovieAddComponent } from './movies/movie-add/movie-add.component'
import { MovieDetailComponent } from './movies/movie-detail/movie-detail.component'
import { MovieEmptyComponent } from './movies/movie-empty/movie-empty.component'

/*SERVICES*/
import { MovieService } from './movies/movie.service'
import { HomeComponent } from './home/home.component'

@NgModule({
    declarations: [
        AppComponent,
        MoviesComponent,
        HeaderComponent,
        MovieAddComponent,
        MovieListComponent,
        MovieTopComponent,
        MovieDetailComponent,
        MovieItemComponent,
        HomeComponent,
        MovieEmptyComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        HttpClientModule
    ],
    providers: [MovieService],
    bootstrap: [AppComponent]
})
export class AppModule { }
