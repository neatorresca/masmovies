import { Component, OnInit } from '@angular/core'
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser'
import { ActivatedRoute, Params } from '@angular/router'

import { Movie } from '../movie.module'
import { MovieService } from '../movie.service'

@Component({
	selector: 'movie-detail',
	templateUrl: './movie-detail.component.html',
	styleUrls: ['./movie-detail.component.less']
})
export class MovieDetailComponent implements OnInit {
	constructor(private _route: ActivatedRoute, private _movieService: MovieService, private _sanitizer:DomSanitizer) { }
	id:number
	movie:Movie
	safeImage: SafeResourceUrl
	
	ngOnInit() {
		this._route.params
			.subscribe(
				(params: Params) =>{
					this.id = +params['id']
					this.movie = this._movieService.getMovie(this.id)
					this.safeImage = this._sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' + this.movie.image)
				}
			)

		this.movie = this._movieService.getMovie(this.id)
		this.safeImage = this._sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg/png;base64,' + this.movie.image)
	}
}
