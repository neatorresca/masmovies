import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-movie-empty',
  templateUrl: './movie-empty.component.html',
  styleUrls: ['./movie-empty.component.less']
})
export class MovieEmptyComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
