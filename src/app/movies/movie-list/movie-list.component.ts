import { Component, OnInit, OnDestroy } from '@angular/core'
import { Subscription } from 'rxjs'
import { Movie } from '../movie.module'
import { MovieService } from '../movie.service'

@Component({
	selector: 'movie-list',
	templateUrl: './movie-list.component.html',
	styleUrls: ['./movie-list.component.less'],
	host:{
		class: 'row'
	}
})
export class MovieListComponent implements OnInit, OnDestroy {
	movies: Movie[];
	subscription: Subscription

	constructor(private _movieService:MovieService) { }

	ngOnInit() {
		this.subscription = this._movieService.movieListChanged
			.subscribe( (movies:Movie[]) =>{
				this.movies = movies;
			})

		this.movies = this._movieService.getMovies()
	}

	ngOnDestroy(){
		this.subscription.unsubscribe();
	}
}