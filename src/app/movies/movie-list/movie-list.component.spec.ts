import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { HomeComponent } from '../../home/home.component'
import { MovieAddComponent } from '../movie-add/movie-add.component'
import { MovieDetailComponent } from '../movie-detail/movie-detail.component'
import { MovieEmptyComponent } from '../movie-empty/movie-empty.component'
import { MovieListComponent } from './movie-list.component'
import { MovieTopComponent } from '../movie-top/movie-top.component'
import { Movie } from '../movie.module'
import { MovieService } from '../movie.service'
import { MovieItemComponent } from './movie-item/movie-item.component'

import { AppRoutingModule } from '../../app-routing.module'
import { FormsModule } from '@angular/forms'
import { HttpClient, HttpHandler } from '@angular/common/http'

describe('MovieListComponent... ', () => {
    let component: MovieListComponent
    let fixture: ComponentFixture<MovieListComponent>

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ 
            	HomeComponent,
            	MovieAddComponent,
            	MovieDetailComponent,
            	MovieEmptyComponent,
            	MovieItemComponent,
            	MovieListComponent,
            	MovieTopComponent
            ],
            imports: [ 
            	AppRoutingModule,
            	FormsModule
            ],
            providers: [ 
            	FormsModule,
            	HttpClient,
            	HttpHandler,
            	MovieService
            ]
        })
        .compileComponents()
    }))

    beforeEach(() => {
        fixture = TestBed.createComponent(MovieListComponent)
        component = fixture.componentInstance
        fixture.detectChanges()
    })

    it('Should create the component MovieListComponent', () => {
        expect(component).toBeTruthy()
    })

    it('Should begin with an empty list of movies', ()=>{
        let DOMCompiled = fixture.debugElement.nativeElement
        expect(DOMCompiled.querySelector('.movie-item__title')).toEqual(null)
    })

    it('Should have a movie after consuming service to add', ()=>{
    	let DOMCompiled = fixture.debugElement.nativeElement,
    		addComponent = TestBed.createComponent(MovieAddComponent).componentInstance

		addComponent.createMockMovie()

		expect(component.movies.length).toEqual(1)
    })
})