import { Component, OnInit, Input } from '@angular/core'
import { Router, ActivatedRoute, Params } from '@angular/router'
import { MovieService } from '../../movie.service'

import {Movie} from '../../movie.module'

@Component({
	selector: 'movie-item',
	templateUrl: './movie-item.component.html',
	styleUrls: ['./movie-item.component.less']
})
export class MovieItemComponent implements OnInit {
	@Input() movie:Movie
	@Input() index:number
	
	constructor(private _movieService:MovieService, private _router:Router, private _route:ActivatedRoute) { }

	ngOnInit() {
	}

	removeItem(index){
		this._movieService.removeMovie(index)
		this._router.navigate(['movies'])
	}

}
