export class Movie{
	public title: string
	public release:Date
	public description: string
	public image: string

	constructor(title:string, release:Date, desc: string, image: string){
		this.title = title
		this.release = new Date(release)
		this.description = desc
		this.image = image
	}
}