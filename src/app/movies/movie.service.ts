import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Subject } from 'rxjs'

import { Movie } from './movie.module'

@Injectable()
export class MovieService {
	movieListChanged = new Subject<Movie[]>()
	private movies:Movie[] = []

	constructor(private _http:HttpClient){}

	addMovie(movie:Movie){
		this.movies.push(movie)
		this.movieListChanged.next(this.movies)
	}

	getMovie(index:number){
		return this.movies[index]
	}

	getMovies(){
		return this.movies.slice()
	}

	getTopMovies(){
		return this._http.get('http://www.mocky.io/v2/5dc3c053300000540034757b')
	}

	removeMovie(index){
		this.movies.splice(index, 1)
		this.movieListChanged.next(this.movies.slice())
	}
}