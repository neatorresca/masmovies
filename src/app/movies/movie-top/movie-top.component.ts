import { Component, OnInit } from '@angular/core'
import { Movie } from '../movie.module'
import { MovieService } from '../movie.service'

@Component({
	selector: 'app-movie-top',
	templateUrl: './movie-top.component.html',
	styleUrls: ['./movie-top.component.less'],
	host:{
		'(window:resize)':'checkSize($event)'
	}
})
export class MovieTopComponent implements OnInit {
	movies:Movie[] = []
	requestError:string = null
	viewType = 'list'

	constructor(private _movieService:MovieService) {}

	ngOnInit() {
		this._movieService.getTopMovies()
			.subscribe(
				result =>{
					this.movies = result['movies']
				},

				error =>{
					this.requestError = 'Error on request, please retry later'
				}
			)

		if(window.innerWidth <= 991){
			this.viewType = 'grid'
		}
	}

	checkSize(event){
		if(event.target.innerWidth <= 991){
			this.viewType = 'grid'
		}
	}
}
