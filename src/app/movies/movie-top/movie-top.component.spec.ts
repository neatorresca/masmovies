import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { MovieTopComponent } from './movie-top.component'
import { Movie } from '../movie.module'
import { MovieService } from '../movie.service'

import { HttpClient, HttpHandler, HttpClientModule } from '@angular/common/http'

describe('MovieTopComponent', () => {
    let component: MovieTopComponent
    let fixture: ComponentFixture<MovieTopComponent>

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ MovieTopComponent ],
            providers: [ 
                HttpClient,
                HttpClientModule,
                HttpHandler,
                MovieService
            ]
        })
        .compileComponents()
    }))

    beforeEach(() => {
        fixture = TestBed.createComponent(MovieTopComponent)
        component = fixture.componentInstance
        fixture.detectChanges()
    })

    afterEach(() => {
        TestBed.resetTestingModule();
    });

    it('Should initialize the component MovieTopComponent', () => {
        expect(component).toBeTruthy()
    })
})