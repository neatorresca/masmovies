import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { HomeComponent } from './home/home.component'
import { MoviesComponent } from './movies/movies.component'
import { MovieAddComponent } from './movies/movie-add/movie-add.component'
import { MovieTopComponent } from './movies/movie-top/movie-top.component'
import { MovieListComponent } from './movies/movie-list/movie-list.component'
import { MovieDetailComponent} from './movies/movie-detail/movie-detail.component'
import { MovieEmptyComponent} from './movies/movie-empty/movie-empty.component'

const routes: Routes = [
	{path: '', redirectTo: '/movies', pathMatch: 'full' },
	{path: 'add', component: MovieAddComponent},
	{path: 'top', component: MovieTopComponent},
	{path: 'movies', component: HomeComponent, children:[
		{path: '', component: MovieEmptyComponent},
		{path: ':id', component: MovieDetailComponent}
	]},
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})

export class AppRoutingModule { }
